#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: runner_install
#
# Copyright 2016, GitLab Inc.
#

case node['platform_family']
when 'debian'
  apt_repository 'gitlab-ci-multi-runner' do
    uri "https://packages.gitlab.com/runner/gitlab-ci-multi-runner/#{node['platform']}"
    components ['main']
    distribution node['lsb']['codename']
    key 'https://packages.gitlab.com/runner/gitlab-ci-multi-runner/gpgkey'
  end
when 'rhel'
  yum_repository 'gitlab-ci-multi-runner' do
    description 'GitLab Runner package server'
    baseurl "https://packages.gitlab.com/runner/gitlab-ci-multi-runner/el/#{node['platform_version'][0]}/$basearch"
    gpgkey 'https://packages.gitlab.com/runner/gitlab-ci-multi-runner/gpgkey'
    gpgcheck false # TODO, change when individual packages get signed
  end
end

package 'gitlab-ci-multi-runner'
