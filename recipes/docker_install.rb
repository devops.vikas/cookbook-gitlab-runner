#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: docker install
#
# Copyright 2016, GitLab Inc.
#

if node['cookbook-gitlab-runner']['docker-engine']['install']
  case node['platform_family']
  when 'debian'
    apt_repository 'docker' do
      uri 'https://apt.dockerproject.org/repo'
      components ['main']
      distribution "#{node['platform']}-#{node['lsb']['codename']}"
      keyserver node['cookbook-gitlab-runner']['docker-engine']['keyserver']
      key node['cookbook-gitlab-runner']['docker-engine']['key']
    end
  when 'rhel'
    yum_repository 'docker' do
      description 'Docker yum repository'
      baseurl 'https://yum.dockerproject.org/repo/main/centos/$releasever/'
      gpgkey 'https://yum.dockerproject.org/gpg'
    end
  end
end

package 'docker-engine'

remote_file '/usr/bin/docker-machine' do
  source "https://github.com/docker/machine/releases/download/v#{node['cookbook-gitlab-runner']['docker-machine']['version']}/docker-machine-Linux-x86_64"
  owner 'root'
  group 'root'
  mode '0755'
  checksum node['cookbook-gitlab-runner']['docker-machine']['checksum']
  only_if { node['cookbook-gitlab-runner']['docker-machine']['install'] }
  not_if "/usr/bin/docker-machine --version | grep #{node['cookbook-gitlab-runner']['docker-machine']['version']}"
end

# Restart Docker after iptables-persistent for nat rules
template '/etc/rc.local' do
  owner  'root'
  group  'root'
  mode   '0755'
end
