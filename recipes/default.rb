#
# Cookbook Name:: cookbook-gitlab-runner
# Recipe:: default
#
# Copyright 2016, GitLab Inc.
#

include_recipe '::docker_install'
include_recipe '::runner_install'
include_recipe '::runner_configure'
