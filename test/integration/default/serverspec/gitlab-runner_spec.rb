require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "GitLab runner" do

  describe package('gitlab-ci-multi-runner') do
    it { should be_installed }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_mode 600 }
  end

  describe file('/etc/gitlab-runner/config.toml') do
    its(:content) { should match /concurrent = 10/ }
    its(:content) { should match /[[runners]]/ }
    its(:content) { should match /[runners.docker]/ }
    its(:content) { should match /image = "ruby:2.1"/ }
    its(:content) { should match /privileged = true/ }
  end
end
