require 'serverspec'

# Required by serverspec
set :backend, :exec

describe "Docker engine" do
  describe package('docker-engine') do
    it { should be_installed }
  end
end
