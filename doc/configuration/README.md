# Configuration

## GitLab Runner

This cookbook will install the latest version of GitLab runner.

It will also allow configuration of the `config.toml` file.

To change the path to `config.toml` file, set:

```
node['cookbook-gitlab-runner']['config_path'] = "/path/to/config.toml"
```

The default is `/etc/gitlab-runner/config.toml`,
see [GitLab runner documentation][] for other possible values.

### config.toml variables

For full set of options check [GitLab runner documentation][].

To change the global section, set:

```
node['cookbook-gitlab-runner']['global_config'] = { "concurrent" => 1 }
```

For example, let us specify the runners section:

```
[[runners]]
  name = "first_runner"
  limit = 20
  url = "https://gitlab.example.com/ci"
  token = "XXX"
  executor = "docker+machine"
  [runners.docker]
    image = "ruby:2.1"
    privileged = true
    volumes = ["/cache", "/usr/local/bundle/gems"]
  [runners.machine]
    IdleCount = 1
    IdleTime = 1800
    MaxBuilds = 50
    MachineDriver = "digitalocean"
    MachineName = "auto-scale-%s"
    MachineOptions = [
      "digitalocean-image=ubuntu",
      "digitalocean-private-networking",
    ]
```

This runner section would translate into configuration that looks like:

```
node['cookbook-gitlab-runner']['runners']['first_runner']['global'] = { "name" => "first_runner", "limit" => 20, "url" => "https://gitlab.example.com/ci", "token" => "XXX", "executor" => "docker+machine" }
node['cookbook-gitlab-runner']['runners']['first_runner']['docker'] = { "image" => "ruby:2.1", "privileged" => true, "volumes" => ['/cache', '/usr/local/bundle/gems']}
node['cookbook-gitlab-runner']['runners']['first_runner']['machine'] = { "IdleCount" => 1, "IdleTime" => 1800, "MaxBuilds" => 50, "MachineDriver" =>  "digitalocean", "MachineName" => "auto-scale-%s", "MachineOptions" =>  ['digitalocean-image=ubuntu', 'digitalocean-private-networking']}
```

Specifying multiple runners section is also possible, for example:

```
[[runners]]
  name = "test1"
  limit = 20
  url = "https://gitlab.example.com/ci"
  [runners.cache]
    Type = "s3"
    AccessKey = "XXX"
    SecretKey = "XXX"
    BucketName = "runner"

[[runners]]
  name = "test2"
  limit = 3
  url = "https://gitlab.example.com/ci"
  token = "XXX"
  [runners.docker]
    image = "ruby:2.1"
    privileged = true
```

would translate into:

```
node['cookbook-gitlab-runner']['runners']['test1']['global'] = { "name" => "test1", "limit" => 20, "url" => "https://gitlab.example.com/ci" }
node['cookbook-gitlab-runner']['runners']['test1']['cache'] = { "Type" => "s3", "AccessKey" => "XXX", "SecretKey" => "XXX", "BucketName" => "runner" }
node['cookbook-gitlab-runner']['runners']['test2']['global'] =  { "name" => "test2", "limit" => 3, "url" => "https://gitlab.example.com/ci", "token" => "XXX" }
node['cookbook-gitlab-runner']['runners']['test2']['docker'] = { "image" => "ruby:2.1", "privileged" => true }

```

## Docker

This cookbook will install docker-engine and docker-machine unless explicitly
disabled.

To disable installation of docker-engine, set:

```
node['cookbook-gitlab-runner']['docker-engine']['install'] = false
```

To disable installation of docker-machine, set:

```
node['cookbook-gitlab-runner']['docker-engine']['install'] = false
```

### Upgrading docker-machine

To change the version of docker-machine, set:

```
node['cookbook-gitlab-runner']['docker-machine']['version'] = "0.7.0"
node['cookbook-gitlab-runner']['docker-machine']['checksum'] = "21e490d5cdfa0d21e543e06b73180614f72e6c18a940f476a64cf084cea23aa5"
```

Note: version is without the `v` prefix. If you change the version you will
need to specify the checksum as well. These details can be found
[here](https://github.com/docker/machine/releases).


[GitLab runner documentation]: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/configuration/advanced-configuration.md#advanced-configuration
