default['cookbook-gitlab-runner']['config_path'] = '/etc/gitlab-runner/config.toml'

default['cookbook-gitlab-runner']['docker-engine']['install'] = true
default['cookbook-gitlab-runner']['docker-engine']['keyserver'] = 'hkp://p80.pool.sks-keyservers.net:80'
default['cookbook-gitlab-runner']['docker-engine']['key'] = '58118E89F3A912897C070ADBF76221572C52609D'

default['cookbook-gitlab-runner']['docker-machine']['install'] = true
default['cookbook-gitlab-runner']['docker-machine']['version'] = "0.7.0"
default['cookbook-gitlab-runner']['docker-machine']['checksum'] = "21e490d5cdfa0d21e543e06b73180614f72e6c18a940f476a64cf084cea23aa5"

default['cookbook-gitlab-runner']['global_config'] = { "concurrent" => 1 }
default['cookbook-gitlab-runner']['runners'] = []
